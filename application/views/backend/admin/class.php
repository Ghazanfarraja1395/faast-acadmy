<hr> 
<div class="panel panel-gradient" >
            
                <div class="panel-heading">
                    <div class="panel-title">
					 <?php echo get_phrase('class_page'); ?>
					</div>
					</div>
<div class="table-responsive">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('class_list');?>
                    	</a></li>
			<li>
            	<a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
					<?php echo get_phrase('add_class');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
		<div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <table  class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		 <th><div><?php echo get_phrase('class_name');?></div></th>
                                 <th><div><?php echo get_phrase('admission_fee');?></div></th>
                                 <th><div><?php echo get_phrase('monthly_fee');?></div></th>
                                 <th><div><?php echo get_phrase('ac_fee');?></div></th>
                                 <th><div><?php echo get_phrase('paper_fee');?></div></th>
                                 <th><div><?php echo get_phrase('total_fee');?></div></th>
                                 <th><div><?php echo get_phrase('options');?></div></th>
				</tr>
			</thead>
                    <tbody>
                    	<?php
                        foreach($classes as $row):?>
                        
                        <tr>
			    <td><?php echo $row['name'];?></td>
                            <td><?php echo $row['admission_fee'];?></td>
                            <td><?php echo $row['monthly_fee'];?></td>
                            <td><?php echo $row['ac_fee'];?></td>
                            <td><?php echo $row['paper_fee'];?></td>
                            <td><?php echo $row['total_fee'];?></td>
                            
                            <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                    
                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_edit_class/<?php echo $row['class_id'];?>');">
                                            <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                            </a>
                                    </li>
                                    <li class="divider"></li>
                                    
                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/classes/delete/<?php echo $row['class_id'];?>');">
                                            <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete');?>
                                            </a>
                                    </li>
                                </ul>
                            </div>
        		    </td>
                            
                            
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
	    </div>
            <!----TABLE LISTING ENDS--->
            
            
			<!----CREATION FORM STARTS---->
			<div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                	<?php echo form_open(base_url() . 'index.php?admin/classes/create' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                        
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('code');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name_numeric" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                          <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('session');?></label>
                                <div class="col-sm-5">
                                    <select name="session_id" class="form-control" style="width:100%;">
                                    	<?php 
										$sessions = $this->db->get('session')->result_array();
										foreach($sessions as $row):
										?>
                                    		<option value="<?php echo $row['session_id'];?>"><?php echo $row['name'];?></option>
                                        <?php
										endforeach;
										?>
                                    </select>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label class="col-sm-3 control-label"><?php /*echo get_phrase('teacher');?></label>
                                <div class="col-sm-5">
                                    <select name="teacher_id" class="form-control" style="width:100%;">
                                    	<?php 
										$teachers = $this->db->get('teacher')->result_array();
										foreach($teachers as $row):
										?>
                                    		<option value="<?php echo $row['teacher_id'];?>"><?php echo $row['name'];?></option>
                                        <?php
										endforeach;*/
										?>
                                    </select>
                                </div>
                            </div>-->
                    <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('admission_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="admission_fee" name="admission_fee"  min="0" onblur="calculateTotalFees()" value="0"/>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('monthly_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="monthly_fee" name="monthly_fee" min="0" onblur="calculateTotalFees()" value="0"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('ac_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="ac_fee" name="ac_fee" min="0" onblur="calculateTotalFees()" value="0" />
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('paper_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="paper_fee" name="paper_fee" onblur="calculateTotalFees()" min="0" value="0"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('total_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="total_fee" name="total_fee"  min="0" readonly data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                              	<div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info"><?php echo get_phrase('add_class');?></button>
                              	</div>
			    </div>
                    </form>                 
                </div>                
			</div>
			<!----CREATION FORM ENDS-->
            
		</div>
	</div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">
    function calculateTotalFees(){
       var admissionFee = document.getElementById('admission_fee').value || 0;
       var monthly_fee = document.getElementById('monthly_fee').value || 0;
       var ac_fee = document.getElementById('ac_fee').value || 0;
       var paper_fee = document.getElementById('paper_fee').value || 0;
       var finalValue = parseInt(admissionFee) + parseInt(monthly_fee) + parseInt(ac_fee) + parseInt(paper_fee)
       //alert (finalValue);
       $("#total_fee").val(finalValue);
    }
	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>