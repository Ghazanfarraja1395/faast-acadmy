
<hr>

 <div class="panel panel-gradient" >
            
                <div class="panel-heading">
                    <div class="panel-title">
					 <?php echo get_phrase('student_fees_information_page'); ?>
					</div>
					</div>
<div class="table-responsive">
<br>
           &nbsp;&nbsp;&nbsp;&nbsp; <a href="<?php echo base_url();?>index.php?admin/add_student_fees/<?php echo $student_id;?>/<?php echo $class_id;?>" class="btn btn-primary ">
                <i class="entypo-plus-circled"></i>
            	<?php echo get_phrase('add_new_fees');?>
                </a> 
                <br><br><br>
               <table class="table table-bordered datatable" id="table_export">
                    <thead>
                        <tr>
                            <th><div><?php echo get_phrase('fees_month');?></div></th>
                            <th><div><?php echo get_phrase('admission_fee');?></div></th>
                            <th><div><?php echo get_phrase('monthly_fee');?></div></th>
                            <th><div><?php echo get_phrase('ac_fee');?></div></th>
                            <th><div><?php echo get_phrase('paper_fee');?></div></th>
                            <th><div><?php echo get_phrase('fees_paid');?></div></th>
                            <th><div><?php echo get_phrase('pending_fee');?></div></th>
                            <th><div><?php echo get_phrase('discount_applied');?></div></th>
                            <th><div><?php echo get_phrase('options');?></div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                
                                foreach($data as $row):?>
                        <tr>
                            <td><?php echo $row['fees_month'];?></td>
                           <!-- <td>    <input type="checkbox" name="proizvodi[]" value="0" <?php //if($row['admission_fee'] === '1') echo 'checked="checked"'; ?>> </td>-->
                            <td><?php echo $row['admission_fee'];?></td>
                            <td><?php echo $row['monthly_fee'];?></td>
                            <td><?php echo $row['ac_fee'];?></td>
                            <td><?php echo $row['paper_fee'];?></td>
                            <td><?php echo $row['fees_paid'];?></td>
                            <td><?php echo $row['fees_pending'];?></td>
                            <td><?php echo $row['discountedAmout'];?></td>
                            
                            <td>
                                
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                            <!-- teacher EDITING LINK -->
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/modal_edit_fees/<?php echo $row['fees_id']; ?>');">
                                                    <i class="entypo-pencil"></i>
                                                    <?php echo get_phrase('edit'); ?>
                                                </a>
                                            </li>
                                            <li class="divider"></li>

                                            <!-- teacher DELETION LINK -->
                                            <li>
                                                <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/teacher/delete/<?php echo $row['fees_id']; ?>');">
                                                    <i class="entypo-trash"></i>
                                                    <?php echo get_phrase('delete'); ?>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/teacher/delete/<?php echo $row['fees_id']; ?>');">
                                                    <i class="entypo-trash"></i>
                                                    <?php echo get_phrase('view'); ?>
                                                </a>
                                            </li>

                                        </ul>
                                </div>
                                
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
</div>
</div>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable({
			"sPaginationType": "bootstrap",
			"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
			"oTableTools": {
				"aButtons": [
					
					{
						"sExtends": "xls",
						"mColumns": [1,2]
					},
					{
						"sExtends": "pdf",
						"mColumns": [1,2]
					},
					{
						"sExtends": "print",
						"fnSetText"	   : "Press 'esc' to return",
						"fnClick": function (nButton, oConfig) {
							datatable.fnSetColumnVis(0, false);
							datatable.fnSetColumnVis(3, false);
							
							this.fnPrint( true, oConfig );
							
							window.print();
							
							$(window).keyup(function(e) {
								  if (e.which == 27) {
									  datatable.fnSetColumnVis(0, true);
									  datatable.fnSetColumnVis(3, true);
								  }
							});
						},
						
					},
				]
			},
			
		});
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>

