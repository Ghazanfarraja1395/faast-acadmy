<?php 
$edit_data=$this->db->get_where('fees' , array('fees_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_fees');?>
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(base_url() . 'index.php?admin/fees/do_update/'.$row['fees_id'] , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('admission_fee');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" min="0" id="admission_fee" name="admission_fee" onblur="calculateTotalFees()" value="<?php echo $row['admission_fee'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('monthly_fee');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control"  id="monthly_fee" name="monthly_fee" onblur="calculateTotalFees()" value="<?php echo $row['monthly_fee'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('ac_fee');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" id="ac_fee" name="ac_fee" onblur="calculateTotalFees()" value="<?php echo $row['ac_fee'];?>"/>
                        </div>
                    </div>
                            <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('paper_fee');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" id="paper_fee" name="paper_fee"  onblur="calculateTotalFees()" value="<?php echo $row['paper_fee'];?>"/>
                        </div>
                    </div>
                            <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('total_fee');?></label>
                        <div class="col-sm-5">
                            <input type="number" class="form-control" id="total_fee" name="total_fee"  readonly value="<?php echo $row['total_fee'];?>"/>
                        </div>
                    </div>  
                  
            		<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo get_phrase('edit_fees');?></button>
						</div>
					</div>
        		</form>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>


<script type="text/javascript">
    
    function calculateTotalFees(){
       var admissionFee = document.getElementById('admission_fee').value || 0;
       var monthly_fee = document.getElementById('monthly_fee').value || 0;
       var ac_fee = document.getElementById('ac_fee').value || 0;
       var paper_fee = document.getElementById('paper_fee').value || 0;
       var finalValue = parseInt(admissionFee) + parseInt(monthly_fee) + parseInt(ac_fee) + parseInt(paper_fee)
       alert (finalValue);
       //$("#total_fee").val(finalValue);
    }
    
</script>