
<style>
    #prrerererere {
    float: right;
}
</style>    
<hr>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-gradient" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('addmission_form'); ?>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/student/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" id="student_name" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus>
                    </div>
                </div>

                <!--<div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php /*echo get_phrase('parent'); ?></label>

                    <div class="col-sm-5">
                        <select name="parent_id" class="form-control select2">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $parents = $this->db->get('parent')->result_array();
                            foreach ($parents as $row):
                                ?>
                                <option value="<?php echo $row['parent_id']; ?>">
                                    <?php echo $row['name']; ?>
                                </option>
                                <?php
                            endforeach; */
                            ?>
                        </select>
                    </div> 
                </div> -->

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('class'); ?></label>

                    <div class="col-sm-5">
                        <select name="class_id" class="form-control" data-validate="required" id="class_id" 
                                data-message-required="<?php echo get_phrase('value_required'); ?>"
                                onchange="return get_class_sections(this.value)">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $classes = $this->db->get('class')->result_array();
                            foreach ($classes as $row):
                                ?>
                            <option  id='takeclasscodevalue' myTag ="<?php echo $row['name_numeric']; ?>" data-class_code="<?php echo $row['name_numeric']; ?>" value="<?php echo $row['class_id']; ?>">
                                    <?php echo $row['name'];?>
                                </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('session'); ?></label>
                    <div class="col-sm-5">
                        <select name="session_id" class="form-control" id="session_id">
                            <option value=""><?php echo get_phrase('select_class_session'); ?></option>
                            <?php
                            $sessions = $this->db->get('session')->result_array();
                            foreach ($sessions as $row):
                                ?>
                                <option id='takesessioncodevalue' mysessionTag= "<?php echo $row['Code']; ?>" value="<?php echo $row['session_id']; ?>">
                                    <?php echo $row['name']; ?>
                                </option>
                                <?php
                            endforeach;
                            ?>   
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('section'); ?></label>
                    <div class="col-sm-5">
                        <select name="section_id" class="form-control section_id" id="section_selector_holder">
                            <option value=""><?php echo get_phrase('select_class_first'); ?></option> 
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('roll'); ?></label>

                    <div class="col-sm-5">
                        <input id="getRollNum" type="text" class="form-control" name="roll" value="" readonly> <span><button id="genRollNmberID" onclick="genRollNmber();" type="button">Generate Roll Number</button> </span>
                    </div> 
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('birthday'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="birthday" value="" data-start-view="2">
                    </div> 
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('gender'); ?></label>

                    <div class="col-sm-5">
                        <select name="sex" class="form-control selectboxit">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <option value="male"><?php echo get_phrase('male'); ?></option>
                            <option value="female"><?php echo get_phrase('female'); ?></option>
                        </select>
                    </div> 
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" value="" >
                    </div> 
                </div>

                <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('phone'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="phone" value="" >
                    </div> 
                </div>
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('photo'); ?></label>

                    <div class="col-sm-5">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                <img src="http://placehold.it/200x200" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                            <div>
                                <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="userfile" accept="image/*">
                                </span>
                                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td colspan="5" style="text-align: center;">Fees Paid</td>
                                <td  style="text-align: center;">Total Fees</td>
                                
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td colspan="5" style="text-align: center;"><input type="checkbox" id="s_ad_fee" name="s_ad_fee" >  <span> Admission Fee</span></td>
                                <td   style="text-align: center;">Admission Fee : <input type="text" id="in_ad_fee" value="0" name="in_ad_fee"><br></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: center;"><input type="checkbox" id="s_mo_fee" name="s_mo_fee" >  Monthly Fee</td>
                                <td  style="text-align: center;">Monthly Fee : <input type="text" id="in_mo_fee" value="0" name="in_mo_fee" ><br></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: center;"><input type="checkbox" id="s_ac_fee" name="s_ac_fee"  >  A/C Fee</td>
                                <td  style="text-align: center;">A/C Fee : <input type="text" id="in_ac_fee" value="0" name="in_ac_fee" ><br></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: center;"><input type="checkbox" id="s_pa_fee" name="s_pa_fee" >  Paper Charges</td>
                                <td  style="text-align: center;">Paper Fee : <input type="text" id="in_pa_fee" value="0" name="in_pa_fee" ><br></td>
                            </tr>
                            <tr>
                                
                                <td>          
                    

                    <div class="col-sm-5">
                        <select id="discount_id" name="discount_id" class="form-control">
                            <option value=0><?php echo get_phrase('select discount if required'); ?></option>
                            <?php
                            $classes = $this->db->get('fees_discount')->result_array();
                            foreach ($classes as $row):
                                ?>
                            <option  feesDiscountValue= "<?php echo $row['amount']; ?>"   value="<?php echo $row['discount_id']; ?>">
                                    <?php echo $row['type'];?>
                            </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                                </td>
                                
                                
                                
                            </tr>
                            
                            
                        </tbody>
                        <tr>
                        
                        </tr>
                    </table>

                </div>
                
                <div class="form-group">
                    <table class="table table-bordered">
                        <tbody>

                            <tr>
                                <td   style="text-align: center;">Total Counted Fee : <input type="text" id="counted_fee" name="counted_fee" value="0" readonly><br></td>
                                <td   style="text-align: center;">Total Paid Fee : <input type="text" id="paid_fee" name="paid_fee"  readonly></td>
                                <td   style="text-align: center;">Fee Balance: <input type="text" id="pending_fee" name="pending_fee" value="0" readonly><br></td>
                                <td><button  onclick="caculateTotalPaidFees();" type="button">Calculate Balance</button></td>
                            </tr>
                            
                        </tbody>
                    </table>

                </div>

                <div class="form-group">
                    <div class="col-sm-6">
                        <h3>Student Account Information</h3> 
                         <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('user_name'); ?></label>
                         <input type="text" class="form-control" name="student_user_name" id="susnam" value="" > <br>
                   
                         <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('password'); ?></label>   
                    
                         <input type="text" class="form-control" id="studentrandomPass" placeholder="Click to generate password" name="password" value="" >
                    
                    </div>
                    <div class="col-sm-6">
                        <h3>Parent Account Information</h3> 
                         <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('user_name'); ?></label>
                         <input type="text" class="form-control" id="pusnam" name="parent_user_name" value="" > <br>
                   
                        <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('password'); ?></label>   
                    
                        <input type="text" class="form-control" name="phone" value="" >
                     
                    </div>
                </div>
                
                
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success btn-sm btn-icon icon-left"> <i class="entypo-plus"></i><?php echo get_phrase('save_student'); ?></button>
                        
                    </div>
                    
                </div>
                
                <?php echo form_close(); ?>
               <!--<form method="post" action="" enctype="multipart/form-data">
                    
                
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php //echo get_phrase('photo'); ?></label>

                    <div class="col-sm-5">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                <img id="profile_image" src="http://placehold.it/200x200" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                            <div>
                                <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" id="userfile" name="userfile" accept="image/*">
                                </span>
                                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                <a href="#" id="upload_student_image" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Upload Image</a>
                            </div>
                        </div>
                    </div>
                </div>
                        <a id="prrerererere"  
                          class="btn btn-primary" target="_blank">
                          <?php //echo get_phrase('print_fees');?>
                       </a>
                   
                    
                </form>-->
                
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    $('#prrerererere').on('click', function() {
    
    var rollNumber = $('#getRollNum').val();
    var className = $('#class_id').find(":selected").text();
    var sessionName = $('#session_id').find(":selected").text();;
    var studentName = $('#student_name').val();
    var admissionFee =  $('#in_ad_fee').val();
    var monthlyfee   =  $('#in_mo_fee').val();
    var acFee =  $('#in_ac_fee').val();
    var paperFee =  $('#in_pa_fee').val();
    var totalPaid =  $('#paid_fee').val();
    var counted_fee =  $('#counted_fee').val();
    var pending_fee =  $('#pending_fee').val();
     var element = $('#discount_id').find('option:selected'); 
     var discountValue = element.attr("feesDiscountValue");
    var discount_percent = discountValue;
    var feesSubmittedBy = '<?php echo $_SESSION['name']?>';
    var queryString = "?rollNumber=" + rollNumber + "&className=" + className  + "&sessionName=" + sessionName + "&studentName=" +studentName + "&admissionFee=" + admissionFee + "&monthlyfee=" +monthlyfee + "&acFee=" +acFee+ "&paperFee=" +paperFee+ "&totalPaid=" +totalPaid+ "&counted_fee=" +counted_fee+ "&feesSubmitedBy="+feesSubmittedBy+"&pending_fee="+pending_fee+"&discount_percent="+discount_percent; 
       //window.location.href = '<?php //echo base_url(); ?>/application/views/backend/admin/student_reciept_fees.php'+queryString+ ','_blank';
       

window.open(
  '<?php echo base_url(); ?>/application/views/backend/admin/student_reciept_fees.php'+queryString,
  '_blank' // <- This is what makes it open in a new window.
);


        
    });




    function get_class_sections(class_id) {
        
        var classCode = $("#takeclasscodevalue").data("class_code");
        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_class_section/' + class_id,
            success: function (response)
            {
                jQuery('#section_selector_holder').html(response);
            }
        });
    }
    
    $(function() { 
    $("#class_id").change(function(){ 
        var element = $(this).find('option:selected'); 
        var myTag = element.attr("myTag");
        var classID = $('#class_id').val();
        var classCode = "";
        if(myTag){
            classCode = myTag+'-';
        }else{
            classCode = 'classCode-';
        }
         
        $('#getRollNum').val(classCode); 
        // send ajax call to get data against class
        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/get_class_details/' + classID,
            success: function (response)
            {
                 var json = JSON.parse(response);
                 
                 $('#s_pa_fee').val(json.paper_fee); 
                 $('#s_ad_fee').val(json.admission_fee); 
                 $('#s_ac_fee').val(json.ac_fee); 
                 $('#s_mo_fee').val(json.monthly_fee);
            }
        });
        
        
    }); 
}); 
    
 $('#discount_id').on('change', function() {
 var element = $(this).find('option:selected'); 
 var discountValue = element.attr("feesDiscountValue");
        //var discountValue =   this.value;
  var counted_fee = $('#s_mo_fee').val(); 
  if( discountValue !== "0"){
      
      if($('#s_mo_fee').is(":checked") === true){
         let discountFormula = counted_fee - (counted_fee * (discountValue / 100));
      $('#counted_fee').val(parseInt(discountFormula));
      }else{
        alert('Discount is applicable only monthly fees!');return false;    
      }
      
  }else{
      location.reload();
      return false;
  }
  
});   
    

    

    
    $(function() { 
    $("#session_id").change(function(){ 
        var element = $(this).find('option:selected'); 
        var mysessionTag = element.attr("mysessionTag");
        var rollVal = $('#getRollNum').val();
        var sessionCode = "";
        if(rollVal){
            var res = rollVal.split("-");
            
            sessionCode = res[0]+'-'+mysessionTag+'-';
        }else{
            alert('Please select class first!');
            return false;
            //sessionCode = 'sessuionCode-';
        }
         
        $('#getRollNum').val(sessionCode); 
    }); 
});    
    
    $('#studentrandomPass').click(function(e) {
     
        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/randomPassword/',
            //  dataType: "json",
            success: function (response)
            {
               $('#studentrandomPass').val(response);
            }
        });
    });
    
    
    
//    $('#upload_student_image').on('click', function() {
//    
//       var rollVal = $('#getRollNum').val();
//       if(!rollVal){
//           alert('Please generate Roll Number First!');
//           return false;
//       }
//        var file_data = $('#userfile').prop('files')[0];
//        var form_data = new FormData();
//        form_data.append('file', file_data);
//        form_data.append('image_name', rollVal);
//        $.ajax({
//                url         : '<?php //echo base_url(); ?>index.php?admin/upload_student_image/'+rollVal, 
//                dataType    : 'text',           
//                cache       : false,
//                contentType : false,
//                processData : false,
//                data        : form_data,                         
//                type        : 'post',
//                success     : function(output){
//                    
//                    if(output){
//                        $('#profile_image').attr('src','<?php //echo base_url(); ?>/uploads/student_image/'+rollVal+'.jpg');
//                    }else{
//                        alert('image uploading failed!');
//                    }
//                    
//                }
//         });
//    });
    
    
$('#s_pa_fee').change(function() {

       var checkValue = $('#s_pa_fee').val();
        if(this.checked) {            
            var existval = $('#counted_fee').val();
            var newval = parseInt(existval) + parseInt(checkValue);
            $('#counted_fee').val(newval);
        }else{
           var existval = $('#counted_fee').val();
           var newval = parseInt(existval) - parseInt(checkValue);
            $('#counted_fee').val(newval);
        }
        
    });
       $('#s_ad_fee').change(function() {
       var checkValue = $('#s_ad_fee').val();
        if(this.checked) {
            var existval = $('#counted_fee').val();
            var newval = parseInt(existval) + parseInt(checkValue);
            $('#counted_fee').val(newval);
        }else{
           var existval = $('#counted_fee').val();
           var newval = parseInt(existval) - parseInt(checkValue);
            $('#counted_fee').val(newval);
        }
        
    });
    $('#s_mo_fee').change(function() {
       var checkValue = $('#s_mo_fee').val();
        if(this.checked) {
            var existval = $('#counted_fee').val();
            var newval = parseInt(existval) + parseInt(checkValue);
            $('#counted_fee').val(newval);
        }else{
           var existval = $('#counted_fee').val();
           var newval = parseInt(existval) - parseInt(checkValue);
            $('#counted_fee').val(newval);
        }
        
    });
     $('#s_ac_fee').change(function() {
       var checkValue = $('#s_ac_fee').val();
        if(this.checked) {
            var existval = $('#counted_fee').val();
            var newval = parseInt(existval) + parseInt(checkValue);
            $('#counted_fee').val(newval);
        }else{
           var existval = $('#counted_fee').val();
           var newval = parseInt(existval) - parseInt(checkValue);
            $('#counted_fee').val(newval);
        }
        
    });


function caculateTotalPaidFees(){
 //alert(value); 
 //alert(id); 
 
 var adFee = $('#in_ad_fee').val();
 var paFee = $('#in_pa_fee').val();
 var acFee = $('#in_ac_fee').val();
 var moFee = $('#in_mo_fee').val();
 var newval = parseInt(adFee) + parseInt(paFee) + parseInt(acFee) + parseInt(moFee);
 $('#paid_fee').val(newval);
 
 var paid_fee = $('#paid_fee').val();
 
    var counted_fee = $('#counted_fee').val();
    var pendingBal = 0;
    
    if(parseInt(paid_fee) < parseInt(counted_fee)){
        
        pendingBal = parseInt(counted_fee) - parseInt(paid_fee);
        
    }else if(parseInt(paid_fee) === parseInt(counted_fee)){
        pendingBal = 0;
    }else{
        alert('this is not valid entry');
        pendingBal = 0;
    }
    
 $('#pending_fee').val(pendingBal);
 
}


function genRollNmber(){
    
      document.getElementById('genRollNmberID').disabled = 'disabled';
              var classId = $('#class_id').val();
        var sessionId = $('#session_id').val();
        var sectionId = $('.section_id').val() || 0;

        if(classId === "" || sessionId === ""){
            alert('Please select class and session first!');       
            return false;
        }
        $.ajax({
            url: '<?php echo base_url(); ?>index.php?admin/generate_roll_num/'+classId+'/'+sessionId+'/'+sectionId,
            //  dataType: "json",
            success: function (response)
            {
                var incCount = parseInt(response) + parseInt(1);
                var rollNoVal = $('#getRollNum').val();
                if(rollNoVal){
                    var newValue  = rollNoVal+incCount;
                    $('#getRollNum').val(newValue);
                    $('#susnam').val(newValue);
                }else{
                   alert('Please select class and session first!');       
                }
            }
        });
}

</script>
