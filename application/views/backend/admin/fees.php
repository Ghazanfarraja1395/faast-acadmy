 <hr>
 <div class="panel panel-gradient" >
            
                <div class="panel-heading">
                    <div class="panel-title">
					 <?php echo get_phrase('manage_fees_information'); ?>
					</div>
					</div>
<div class="table-responsive">
				
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('fees_list');?>
                    	</a></li>
			<li>
            	<a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
					<?php echo get_phrase('add_fees');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
        
		<div class="tab-content">
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
				
<table class="table table-bordered table-striped datatable" id="table-2">
                	<thead>
                		<tr>
                    		<th><div>#</div></th>
                    		<th><div><?php echo get_phrase('admission_fee');?></div></th>
                                <th><div><?php echo get_phrase('monthly_fee');?></div></th>
                                <th><div><?php echo get_phrase('ac_fee');?></div></th>
                                <th><div><?php echo get_phrase('paper_fee');?></div></th>
                                <th><div><?php echo get_phrase('total_fee');?></div></th>
                    		<th><div><?php echo get_phrase('options');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($fees as $row):?>
                        <tr>
                            <td><?php echo $count++;?></td>
							<td><?php echo $row['admission_fee'];?></td>
                                                        <td><?php echo $row['monthly_fee'];?></td>
                                                        <td><?php echo $row['ac_fee'];?></td>
                                                        <td><?php echo $row['paper_fee'];?></td>
                                                        <td><?php echo $row['total_fee'];?></td>
							<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-blue btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-primary pull-right" role="menu">
                                    
                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/modal_edit_fees/<?php echo $row['fees_id'];?>');">
                                            <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                            </a>
                                                    </li>
                                    <li class="divider"></li>
                                    
                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/fees/delete/<?php echo $row['fees_id'];?>');">
                                            <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete');?>
                                            </a>
                                                    </li>
                                </ul>
                            </div>
        					</td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
            
            
			<!----CREATION FORM STARTS---->
			<div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                	<?php echo form_open(base_url() . 'index.php?admin/fees/create' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                        <div class="padded">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('admission_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="admission_fee" name="admission_fee"  min="0" onblur="calculateTotalFees()" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('monthly_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="monthly_fee" name="monthly_fee" min="0" onblur="calculateTotalFees()" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('ac_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="ac_fee" name="ac_fee" min="0" onblur="calculateTotalFees()" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('paper_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="paper_fee" name="paper_fee" onblur="calculateTotalFees()" min="0" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('total_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="total_fee" name="total_fee"  min="0" readonly data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            
                            
                        <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-blue btn-sm btn-icon icon-left"><i class="fa fa-save"></i>&nbsp;<?php echo get_phrase('add_fees');?></button>
                              </div>
			</div>
                    </form>                
                </div>                
			</div>
			<!----CREATION FORM ENDS-->
		</div>
	</div>
</div>

</div>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">tal
    
    function calculateTotalFees(){
       var admissionFee = document.getElementById('admission_fee').value || 0;
       var monthly_fee = document.getElementById('monthly_fee').value || 0;
       var ac_fee = document.getElementById('ac_fee').value || 0;
       var paper_fee = document.getElementById('paper_fee').value || 0;
       var finalValue = parseInt(admissionFee) + parseInt(monthly_fee) + parseInt(ac_fee) + parseInt(paper_fee)
       //alert (finalValue);
       $("#total_fee").val(finalValue);
    }
    
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>