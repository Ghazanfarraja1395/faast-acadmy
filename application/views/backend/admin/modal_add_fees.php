<?php  //print_r($param3); die();?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('add_fees');?>
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(base_url() . 'index.php?admin/fees/create/' , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                   <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('fees_month');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control datepicker" name="fees_month" value="" data-start-view="2">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('admission_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="admission_fee" name="admission_fee"  min="0" onblur="calculateTotalFees()" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('monthly_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="monthly_fee" name="monthly_fee" min="0" onblur="calculateTotalFees()" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('ac_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="ac_fee" name="ac_fee" min="0" onblur="calculateTotalFees()" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('paper_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="paper_fee" name="paper_fee" onblur="calculateTotalFees()" min="0" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('total_fee');?></label>
                                <div class="col-sm-5">
                                    <input type="number" class="form-control" id="total_fee" name="total_fee"  min="0" readonly data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                  <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('status'); ?></label>

                    <div class="col-sm-5">
                        <select name="status" class="form-control select2">
                            <option value=""><?php echo get_phrase('select_status'); ?></option>
<option value="paid"><?php echo get_phrase('paid'); ?></option>
<option value="pending"><?php echo get_phrase('pending'); ?></option>
                        </select>
                    </div>
                    <input type="hidden" name="student_id" value="<?php echo $param2;?>" />
                </div>
                            <div class="form-group">
                                 <table style="width:100%">
  <tr>
    <th>Firstname</th>
    <th>Lastname</th>
    <th>Age</th>
  </tr>
  <tr>
    <td>Jill</td>
    <td>Smith</td>
    <td>50</td>
  </tr>
  <tr>
    <td>Eve</td>
    <td>Jackson</td>
    <td>94</td>
  </tr>
</table> 
                            </div>            
            		<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo get_phrase('add_fees');?></button>
						</div>
					</div>
        		</form>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    
    function calculateTotalFees(){
       var admissionFee = document.getElementById('admission_fee').value || 0;
       var monthly_fee = document.getElementById('monthly_fee').value || 0;
       var ac_fee = document.getElementById('ac_fee').value || 0;
       var paper_fee = document.getElementById('paper_fee').value || 0;
       var finalValue = parseInt(admissionFee) + parseInt(monthly_fee) + parseInt(ac_fee) + parseInt(paper_fee)
       //alert (finalValue);
       $("#total_fee").val(finalValue);
    }
    
</script>