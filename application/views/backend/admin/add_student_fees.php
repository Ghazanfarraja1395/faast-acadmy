<?php //echo 'data';
//print_r($dataforFees);
//echo $dataforFees[0]['monthly_fee']; ?>

<style>
    .exam_chart {
        width           : 100%;
        height      : 265px;
        font-size   : 11px;
    }

</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class="entypo-plus-circled"></i>
<?php echo get_phrase('add_fees'); ?>
                </div>
            </div>
            <div class="panel-body">

<?php echo form_open(base_url() . 'index.php?admin/fees/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('fees_month'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control datepicker" name="fees_month" value="" data-start-view="2">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('student_name'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="class_name" value="<?php echo $dataforFees[0]['student_name']?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('class_name'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="class_name" value="<?php echo $dataforFees[0]['class_name']?>" >
                    </div>
                </div>
                
                            <div class="form-group">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td colspan="5" style="text-align: center;">Fees Paid</td>
                                <td  style="text-align: center;">Total Fees</td>
                                
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td colspan="5" style="text-align: center;"><input type="checkbox" id="s_ad_fee" value="<?php echo $dataforFees[0]['admission_fee']?>" name="s_ad_fee" >  <span> Admission Fee</span></td>
                                <td   style="text-align: center;">Admission Fee : <input type="text" id="in_ad_fee" value="0" name="in_ad_fee"><br></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: center;"><input type="checkbox" id="s_mo_fee" value="<?php echo $dataforFees[0]['monthly_fee']?>" name="s_mo_fee" >  Monthly Fee</td>
                                <td  style="text-align: center;">Monthly Fee : <input type="text" id="in_mo_fee" value="0" name="in_mo_fee" ><br></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: center;"><input type="checkbox" id="s_ac_fee" value="<?php echo $dataforFees[0]['ac_fee']?>" name="s_ac_fee"  >  A/C Fee</td>
                                <td  style="text-align: center;">A/C Fee : <input type="text" id="in_ac_fee" value="0" name="in_ac_fee" ><br></td>
                            </tr>
                            <tr>
                                <td colspan="5" style="text-align: center;"><input type="checkbox" id="s_pa_fee" value="<?php echo $dataforFees[0]['paper_fee']?>" name="s_pa_fee" >  Paper Charges</td>
                                <td  style="text-align: center;">Paper Fee : <input type="text" id="in_pa_fee" value="0" name="in_pa_fee" ><br></td>
                            </tr>
                            <tr>
                                
                                <td>          
                    

                    <div class="col-sm-5">
                        <select id="discount_id" name="discount_id" class="form-control">
                            <option value=0><?php echo get_phrase('select discount if required'); ?></option>
                            <?php
                            $classes = $this->db->get('fees_discount')->result_array();
                            foreach ($classes as $row):
                                ?>
                            <option  feesDiscountValue= "<?php echo $row['amount']; ?>"   value="<?php echo $row['discount_id']; ?>">
                                    <?php echo $row['type'];?>
                            </option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div> 
                                </td>
                            </tr>      
                        </tbody>
                        <tr>
                        
                        </tr>
                    </table>

                </div>    
                
                                <div class="form-group">
                    <table class="table table-bordered">
                        <tbody>

                            <tr>
                                <td   style="text-align: center;">Total Counted Fee : <input type="text" id="counted_fee" name="counted_fee" value="0" readonly><br></td>
                                <td   style="text-align: center;">Total Paid Fee : <input type="text" id="paid_fee" name="paid_fee"  readonly></td>
                                <td   style="text-align: center;">Fee Balance: <input type="text" id="pending_fee" name="pending_fee" value="0" readonly><br></td>
                                <td><button  onclick="caculateTotalPaidFees();" type="button">Calculate Balance</button></td>
                            </tr>
                            
                        </tbody>
                    </table>

                </div>
                <input type="hidden" name="student_id" value="<?php echo $dataforFees[0]['student_id']; ?>" >
                <input type="hidden" name="class_id" value="<?php echo $dataforFees[0]['class_id']; ?>" >
                <input type="hidden" name="session_id" value="<?php echo $dataforFees[0]['session_id']; ?>" >
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('add_fees'); ?></button>
                    </div>
                </div>
                    <a href="<?php echo base_url(); ?>index.php?admin/student_marksheet_print_view/<?php echo $student_id; ?>/<?php echo $row2['exam_id']; ?>" 
                       class="btn btn-primary" target="_blank">
<?php echo get_phrase('print_reciept'); ?>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>




<script type="text/javascript">

   
function caculateTotalPaidFees(){
 //alert(value); 
 //alert(id); 
 
 var adFee = $('#in_ad_fee').val();
 var paFee = $('#in_pa_fee').val();
 var acFee = $('#in_ac_fee').val();
 var moFee = $('#in_mo_fee').val();
 var newval = parseInt(adFee) + parseInt(paFee) + parseInt(acFee) + parseInt(moFee);
 $('#paid_fee').val(newval);
 
 var paid_fee = $('#paid_fee').val();
 
    var counted_fee = $('#counted_fee').val();
    var pendingBal = 0;
    
    if(parseInt(paid_fee) < parseInt(counted_fee)){
        
        pendingBal = parseInt(counted_fee) - parseInt(paid_fee);
        
    }else if(parseInt(paid_fee) === parseInt(counted_fee)){
        pendingBal = 0;
    }else{
        alert('this is not valid entry');
        pendingBal = 0;
    }
    
 $('#pending_fee').val(pendingBal);
 
}

$('#s_pa_fee').change(function() {

       var checkValue = $('#s_pa_fee').val();
        if(this.checked) {            
            var existval = $('#counted_fee').val();
            var newval = parseInt(existval) + parseInt(checkValue);
            $('#counted_fee').val(newval);
        }else{
           var existval = $('#counted_fee').val();
           var newval = parseInt(existval) - parseInt(checkValue);
            $('#counted_fee').val(newval);
        }
        
    });
       $('#s_ad_fee').change(function() {
       var checkValue = $('#s_ad_fee').val();
        if(this.checked) {
            var existval = $('#counted_fee').val();
            var newval = parseInt(existval) + parseInt(checkValue);
            $('#counted_fee').val(newval);
        }else{
           var existval = $('#counted_fee').val();
           var newval = parseInt(existval) - parseInt(checkValue);
            $('#counted_fee').val(newval);
        }
        
    });
    $('#s_mo_fee').change(function() {
       var checkValue = $('#s_mo_fee').val();
        if(this.checked) {
            var existval = $('#counted_fee').val();
            var newval = parseInt(existval) + parseInt(checkValue);
            $('#counted_fee').val(newval);
        }else{
           var existval = $('#counted_fee').val();
           var newval = parseInt(existval) - parseInt(checkValue);
            $('#counted_fee').val(newval);
        }
        
    });
     $('#s_ac_fee').change(function() {
       var checkValue = $('#s_ac_fee').val();
        if(this.checked) {
            var existval = $('#counted_fee').val();
            var newval = parseInt(existval) + parseInt(checkValue);
            $('#counted_fee').val(newval);
        }else{
           var existval = $('#counted_fee').val();
           var newval = parseInt(existval) - parseInt(checkValue);
            $('#counted_fee').val(newval);
        }
        
    });


</script>